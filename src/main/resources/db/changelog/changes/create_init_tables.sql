-- liquibase formatted sql

-- changeset mersudin:1
-- comment: create user, address, category, shopping_cart, order, product and shopping_cart_item

CREATE TABLE "user" (
    id BIGSERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(256) NOT NULL,
    is_admin BOOLEAN NOT NULL ,
    PRIMARY KEY (id)
);

CREATE TABLE address (
    id BIGSERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    address_line1 VARCHAR(256) NOT NULL,
    address_line2 VARCHAR(256) NOT NULL,
    city VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE category (
    id BIGSERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE shopping_cart (
    id BIGSERIAL NOT NULL,
    date_created DATE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE "order" (
    id BIGSERIAL NOT NULL,
    user_id BIGINT NOT NULL REFERENCES "user" (id),
    date_placed DATE NOT NULL,
    shopping_cart_id BIGINT NOT NULL REFERENCES "shopping_cart" (id),
    shipping_address_id BIGINT NOT NULL REFERENCES "address" (id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);

CREATE TABLE product (
    id BIGSERIAL NOT NULL,
    title VARCHAR(50) NOT NULL,
    price float8 NOT NULL,
    category VARCHAR(50) NOT NULL,
    image_url TEXT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE shopping_cart_item (
    id BIGSERIAL NOT NULL,
    shopping_cart_id BIGINT NOT NULL REFERENCES "shopping_cart" (id),
    product_id BIGINT NOT NULL REFERENCES "product" (id) ON DELETE CASCADE,
    quantity INT NOT NULL,
    PRIMARY KEY (id)
);