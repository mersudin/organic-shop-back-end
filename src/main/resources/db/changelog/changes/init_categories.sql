-- liquibase formatted sql

-- changeset mersudin:2
-- comment: save initial categories

INSERT INTO category (name) VALUES ('Sweets');
INSERT INTO category (name) VALUES ('Juices');
INSERT INTO category (name) VALUES ('Dairy');
INSERT INTO category (name) VALUES ('Frozen');
INSERT INTO category (name) VALUES ('Fruits');
INSERT INTO category (name) VALUES ('Vegetables');
INSERT INTO category (name) VALUES ('Canned');
INSERT INTO category (name) VALUES ('Spices');
INSERT INTO category (name) VALUES ('Pasta');
INSERT INTO category (name) VALUES ('Bread');
INSERT INTO category (name) VALUES ('Pets');

