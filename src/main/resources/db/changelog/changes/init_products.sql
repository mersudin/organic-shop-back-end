-- liquibase formatted sql

-- changeset mersudin:3
-- comment: save initial products

INSERT INTO product (title, price, category, image_url) VALUES ('Milka Alpine milk', 2.5, 'Sweets', 'https://static-01.daraz.pk/p/4d537564e3dcdd9d3994d9b70d3c7906.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Milka Brownie', 4.5, 'Sweets', 'https://d17zv3ray5yxvp.cloudfront.net/variants/sCZYHeE4yHKwFKUwCPHwPXDM/57ed05bea98bceae5f0eaada26b69cee6c61471d3030f7123d212844a35eba04');
INSERT INTO product (title, price, category, image_url) VALUES ('Nutella', 9.5, 'Sweets', 'https://d17zv3ray5yxvp.cloudfront.net/variants/LgCdRjkG1AHYZcD5hLBEA6D5/57ed05bea98bceae5f0eaada26b69cee6c61471d3030f7123d212844a35eba04');
INSERT INTO product (title, price, category, image_url) VALUES ('m&m', 3.5, 'Sweets', 'https://m.media-amazon.com/images/I/71RNc7ru+nL.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Coca Cola', 2.5, 'Juices', 'https://online.idea.rs/images/products/473/473103695_1l.jpg?1548695463');
INSERT INTO product (title, price, category, image_url) VALUES ('Coca Cola can', 1.5, 'Juices', 'https://shop.tesco.ba/wp-content/uploads/2021/06/coca-cola-250-ml.png');
INSERT INTO product (title, price, category, image_url) VALUES ('Cappy orange', 2.5, 'Juices', 'https://www.dragstor.ba/wp-content/uploads/2022/04/A0063.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Fructal strawberry', 2.5, 'Juices', 'https://static.wixstatic.com/media/7afb08_a43903aa168a43eeb36447806515b5d2~mv2.jpg/v1/fill/w_560,h_560,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/7afb08_a43903aa168a43eeb36447806515b5d2~mv2.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Milk', 1.8, 'Dairy', 'https://www.dairyfoods.com/ext/resources/DF/2020/August/df-100/GettyImages-1194287257.jpg?1597726305');
INSERT INTO product (title, price, category, image_url) VALUES ('Dairy Mix', 5.5, 'Dairy', 'https://domf5oio6qrcr.cloudfront.net/medialibrary/9685/iStock-544807136.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Madi cevap', 6.5, 'Frozen', 'https://www.madi.ba/wp-content/uploads/2017/10/Pile%C4%87i-%C4%87evap%C4%8Di%C4%87i-400g.png');
INSERT INTO product (title, price, category, image_url) VALUES ('Pizza', 5.5, 'Frozen', 'https://isplatise.ba/wp-content/uploads/2021/04/product-image-16.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Fish', 6.5, 'Frozen', 'https://www.gannett-cdn.com/presto/2019/04/08/PDTF/f708e68b-3af8-486a-811c-a6a5e3650961-gortons.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Blueberry', 4.5, 'Fruits', 'https://www.tastingtable.com/img/gallery/15-types-of-blueberries-and-what-makes-them-unique/intro-1656439699.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Avocado', 2.5, 'Fruits', 'https://www.tastingtable.com/img/gallery/is-it-dangerous-to-eat-the-skin-on-avocados/l-intro-1651422942.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Banana', 3, 'Fruits', 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Bananas.jpg/1024px-Bananas.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Orange', 2.5, 'Fruits', 'https://upload.wikimedia.org/wikipedia/commons/c/c4/Orange-Fruit-Pieces.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Apple', 2.6, 'Fruits', 'https://www.osfhealthcare.org/blog/wp-content/uploads/2019/08/apples-OG.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Grape', 4.3, 'Fruits', 'https://upload.wikimedia.org/wikipedia/commons/3/36/Kyoho-grape.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Peach', 2, 'Fruits', 'https://upload.wikimedia.org/wikipedia/commons/9/9e/Autumn_Red_peaches.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Strawberry', 3.8, 'Fruits', 'https://upload.wikimedia.org/wikipedia/commons/e/e1/Strawberries.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Tomato', 2, 'Vegetables', 'https://static.pexels.com/photos/8390/food-wood-tomatoes.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Lettuce', 1, 'Vegetables', 'https://upload.wikimedia.org/wikipedia/commons/7/7f/Lettuce_Mini_Heads_%287331119710%29.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Cauliflower', 1.2, 'Vegetables', 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Cauliflowers_-_20051021.jpg/1280px-Cauliflowers_-_20051021.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Spinach', 1.5, 'Vegetables', 'http://www.publicdomainpictures.net/pictures/170000/velka/spinach-leaves-1461774375kTU.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Tuna', 1.5, 'Canned', 'https://m.media-amazon.com/images/I/81v0cFrxVYL._SL1500_.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Fish fillets', 2.5, 'Canned', 'https://www.sealord.com/media/uollzhbj/10673-sealord-ambient-can-renders-smoked-fish-fillet-450g-%C6%9203.png');
INSERT INTO product (title, price, category, image_url) VALUES ('Chicken cutlets', 2.5, 'Canned', 'https://m.media-amazon.com/images/I/81v+lKDu7PL.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Spicy Mix', 2.3, 'Spices', 'https://static.oxinis.com/healthmug/image/blog/article/19-banner-700.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Cinnamon Sticks', 1.4, 'Spices', 'https://upload.wikimedia.org/wikipedia/commons/8/8c/Cinnamon-other.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Saffron', 3.5, 'Spices', 'https://upload.wikimedia.org/wikipedia/commons/4/48/Saffron_Crop.JPG');
INSERT INTO product (title, price, category, image_url) VALUES ('Ground Turmeric', 3, 'Spices', 'http://maxpixel.freegreatpicture.com/static/photo/1x/Seasoning-Powder-Curry-Spice-Ingredient-Turmeric-2344157.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Coriander Seeds', 1.2, 'Spices', 'http://maxpixel.freegreatpicture.com/static/photo/1x/Ingredient-Herb-Seasoning-Seeds-Food-Coriander-390015.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Macaroni', 2.2, 'Pasta', 'https://aldprdproductimages.azureedge.net/media/resized/$Aldi_IE/Resize%2031.05.22/4088600300030_0_L.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Chicken noodles', 6.4, 'Pasta', 'https://files.ekmcdn.com/asiancookshop/images/indomie-chicken-flavour-instant-noodle-16716-p.jpeg?v=CD038179-3175-4CA6-B74A-2B7C69CA32A3');
INSERT INTO product (title, price, category, image_url) VALUES ('Noodle soup', 3.5, 'Pasta', 'https://cdn.shopify.com/s/files/1/0565/6467/8848/products/snapshotimagehandler_1929544687_580x.jpg?v=1632465714');
INSERT INTO product (title, price, category, image_url) VALUES ('Freshly Baked Bread', 2, 'Bread', 'https://static.pexels.com/photos/2434/bread-food-healthy-breakfast.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Lavish Bread', 2, 'Bread', 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Fabrication_du_lavash_%C3%A0_Noravank_%286%29.jpg/1280px-Fabrication_du_lavash_%C3%A0_Noravank_%286%29.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Bagel Bread', 2.7, 'Bread', 'https://cdn.loveandlemons.com/wp-content/uploads/2020/05/bagel-recipe.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Baguette Bread', 1.2, 'Bread', 'https://static.pexels.com/photos/416607/pexels-photo-416607.jpeg');
INSERT INTO product (title, price, category, image_url) VALUES ('Dog food', 7.2, 'Pets', 'https://happydog_en_sb.cstatic.io/440x440/f/69110/480x480/c90b13149a/hd-vet-new-product-slider-packshots-sensible-11kg-300g.png');
INSERT INTO product (title, price, category, image_url) VALUES ('Cat food', 6.6, 'Pets', 'https://m.media-amazon.com/images/I/61HbA7XklpL._SL1000_.jpg');
INSERT INTO product (title, price, category, image_url) VALUES ('Parrot food', 3.5, 'Pets', 'https://i5.walmartimages.com/asr/34ca3b09-5938-4908-bea4-f6d690754b03.240a8d2df3f60986c9660ac1e0ce950b.jpeg');
