-- liquibase formatted sql

-- changeset mersudin:4
-- comment: save two users with encrypted passwords, admin psw: "a", user psw: "u"

INSERT INTO "user" (name, email, password, is_admin) VALUES ('Admin', 'a', '$2a$10$L.NDifrFrWtOCMDHe42i5OYjS0a6T1Vm6HcFNAnCAlRTh5u5i1Gp.', true);
INSERT INTO "user" (name, email, password, is_admin) VALUES ('User', 'u', '$2a$10$TzgmECHEbB/SQk1NYkYz0uYGfJXxbMhq0EBIthnw1yRKtAJy3GiZy', false);