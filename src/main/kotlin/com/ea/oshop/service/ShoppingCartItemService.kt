package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.ShoppingCartItemRepository
import com.ea.oshop.dto.ShoppingCartItemDTO
import com.ea.oshop.model.ShoppingCartItem
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service

@Service
class ShoppingCartItemService(
    @Qualifier("itemDataSource") private val itemRepository: ShoppingCartItemRepository,
    private val productService: ProductService
) {
    fun getAllItems(): List<ShoppingCartItemDTO> {
        val items = itemRepository.findAll()
        return convertAllItemsEntityToDto(items)
    }

    fun getItemsByCartId(cartId: Long): List<ShoppingCartItemDTO> {
        val list = itemRepository.findByShoppingCartId(cartId)
        return convertAllItemsEntityToDto(list)
    }

    fun getItemsByShoppingCartId(id: Long) : List<ShoppingCartItem> = itemRepository.findByShoppingCartId(id)

    fun getItem(cartId: Long, productId: Long) : ShoppingCartItemDTO {
        val item = itemRepository.findByShoppingCartIdAndProductId(cartId, productId)
        return convertItemEntityToDto(item)
    }

    fun addItem(cartId: Long, productId: Long) : ShoppingCartItemDTO {
        if(itemRepository.exists(cartId,productId)) {
            val item = itemRepository.findByShoppingCartIdAndProductId(cartId, productId)
            ++item.quantity
            itemRepository.save(item)
            return convertItemEntityToDto(item)
        }
        val newItem = ShoppingCartItem(shoppingCartId = cartId, productId = productId, quantity = 1)
        return convertItemEntityToDto(itemRepository.save(newItem))
    }

    fun deleteAllItems() = itemRepository.deleteAll()

    fun deleteItem(cartId: Long, productId: Long) : Boolean {
        val item = itemRepository.findByShoppingCartIdAndProductId(cartId, productId);
        if(item.quantity > 1){
            --item.quantity;
            itemRepository.save(item);
            return false
        }
        itemRepository.delete(item);
        return true
    }

    fun deleteItemsByCartId(id: Long) {
        val items = itemRepository.findByShoppingCartId(id)
        items.forEach { i -> itemRepository.delete(i) }
    }

    fun convertItemEntityToDto(item: ShoppingCartItem): ShoppingCartItemDTO {
        val p = productService.getProductById(item.productId)
        return ShoppingCartItemDTO(id = item.id!!,  product = p, quantity = item.quantity)
    }
    fun convertAllItemsEntityToDto(items: List<ShoppingCartItem>): List<ShoppingCartItemDTO> {
        return items.map { i -> convertItemEntityToDto(i) }
    }
}