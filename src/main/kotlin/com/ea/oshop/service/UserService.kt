package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.UserRepository
import com.ea.oshop.dto.CredentialsDTO
import com.ea.oshop.model.User
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserService(@Qualifier("userDataSource") private val userRepository: UserRepository) {
//class UserService(@Qualifier("mockUserDataSource") private val userRepository: UserRepository) {

    fun getUsers(): List<User> = userRepository.findAll()

    fun getUser(id: Long) : User = userRepository.findById(id)

    fun existUserByEmail(email: String) : Boolean = userRepository.existsByEmail(email)

    fun register(user: User): String {
        user.password = encodePassword(user.password)
        val saved = userRepository.save(user)
        return createJWTtoken(saved)
    }

    fun login(credentialsDTO: CredentialsDTO) : String {
        val user = userRepository.findByEmail(credentialsDTO.email)
        if(!passwordsMatch(credentialsDTO.password, user.password))
            throw IllegalArgumentException("Incorrect password.")
        return createJWTtoken(user)
    }

    fun deleteUser(id: Long) = userRepository.deleteById(id);

    private fun passwordsMatch(pwd1: String, pwd2: String) : Boolean = BCryptPasswordEncoder().matches(pwd1,pwd2)

    private fun encodePassword (psw: String): String {
        val passwordEncoder = BCryptPasswordEncoder()
        return passwordEncoder.encode(psw)
    }

    private fun createJWTtoken(user: User) : String {
        return Jwts.builder()
            .claim("id", user.id)
            .claim("name", user.name)
            .claim("email", user.email)
            .claim("isAdmin", user.isAdmin)
            .setExpiration(Date(System.currentTimeMillis() + 60*1000*10))
            .signWith(SignatureAlgorithm.HS256, "TODOSAFER").compact()
    }

}