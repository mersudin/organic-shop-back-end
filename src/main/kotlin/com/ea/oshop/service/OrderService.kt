package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.OrderRepository
import com.ea.oshop.dto.OrderDTO
import com.ea.oshop.model.Order
import org.springframework.stereotype.Service
import java.time.LocalDate


@Service
class OrderService (
    private val orderRepository: OrderRepository,
    private val shoppingCartService: ShoppingCartService,
    private val addressService: AddressService
    ) {

    fun addOrder(order: OrderDTO): OrderDTO {
        val orderEntity = convertOrderDtoToEntity(order);
        val savedOrder = orderRepository.save(orderEntity)

        return convertOrderEntityToDto(savedOrder)
    }

    fun getOrder(orderId: Long) : OrderDTO {
        val order = this.orderRepository.findById(orderId).get()
        return convertOrderEntityToDto(order)
    }

    fun getAllOrders() : List<OrderDTO> {
        val orders = this.orderRepository.findAll()
        return convertAllOrderEntitiesToDto(orders)
    }

    fun getOrdersByUser(userId: Long): List<OrderDTO> {
        val userOrders = this.orderRepository.findByUserId(userId)
        return convertAllOrderEntitiesToDto(userOrders)
    }

    private fun convertOrderDtoToEntity(order: OrderDTO) : Order {
        val addressId = this.addressService.addAddress(order.shippingAddress).id
        val date = LocalDate.parse(order.datePlaced)

        return Order(order.id, order.userId, date, order.shoppingCart.id, addressId)
    }

    private fun convertOrderEntityToDto(order: Order) : OrderDTO {
        val cart = shoppingCartService.getShoppingCart(order.shoppingCartId)
        val address = this.addressService.findById(order.shippingAddressId)

        return OrderDTO(order.id, order.userId, order.datePlaced.toString(), cart, address)
    }

    private fun convertAllOrderEntitiesToDto(orders: List<Order>) : List<OrderDTO> {
        return orders.map { i -> convertOrderEntityToDto(i) }
    }

}

