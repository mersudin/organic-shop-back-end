package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.AddressRepository
import com.ea.oshop.model.Address
import org.springframework.stereotype.Service

@Service
class AddressService(private val addressRepository: AddressRepository) {

    fun addAddress(address: Address) : Address = addressRepository.save(address)

    fun findById(id: Long) : Address = addressRepository.findById(id).get()

}