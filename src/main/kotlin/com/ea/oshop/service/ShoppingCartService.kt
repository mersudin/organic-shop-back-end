package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.ShoppingCartRepository
import com.ea.oshop.dto.ShoppingCartDTO
import com.ea.oshop.model.ShoppingCart
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class ShoppingCartService(
//    @Qualifier("mockCartDataSource") private val shoppingCartRepository: ShoppingCartRepository,
    @Qualifier("cartDataSource") private val shoppingCartRepository: ShoppingCartRepository,
    private val shoppingCartItemService: ShoppingCartItemService
    ) {

    fun addShoppingCart() : ShoppingCartDTO {
        val cart = shoppingCartRepository.save(ShoppingCart(dateCreated = LocalDate.now()))
        return convertCartEntityToDto(cart)
    }

    fun getShoppingCart(id: Long): ShoppingCartDTO {
        val cart = shoppingCartRepository.findById(id)
        return convertCartEntityToDto(cart)
    }

    fun getShoppingCarts(): List<ShoppingCartDTO> {
        val carts = shoppingCartRepository.findAll()
        return convertAllCartEntitiesToDto(carts)
    }

    fun clearShoppingCart(id: Long): ShoppingCartDTO {
        shoppingCartItemService.deleteItemsByCartId(id)
        val cart = shoppingCartRepository.findById(id);
        return convertCartEntityToDto(cart)
    }

    private fun convertCartEntityToDto(cart: ShoppingCart): ShoppingCartDTO {
        val items = shoppingCartItemService.getItemsByShoppingCartId(cart.id);
        val itemsDto = shoppingCartItemService.convertAllItemsEntityToDto(items)
        return ShoppingCartDTO(id = cart.id, dateCreated = cart.dateCreated, items = itemsDto)
    }

    private fun convertAllCartEntitiesToDto(carts: List<ShoppingCart>) : List<ShoppingCartDTO> {
        val shoppingCartDTOs = mutableListOf<ShoppingCartDTO>()
        for (cart in carts) {
            val items = shoppingCartItemService.getItemsByShoppingCartId(cart.id);
            val itemsDto = shoppingCartItemService.convertAllItemsEntityToDto(items)
            val shoppingCartDto = ShoppingCartDTO(id = cart.id, dateCreated = cart.dateCreated, items = itemsDto)
            shoppingCartDTOs.add(shoppingCartDto)
        }
        return shoppingCartDTOs
    }

}