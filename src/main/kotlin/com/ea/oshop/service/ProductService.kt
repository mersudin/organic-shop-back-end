package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.ProductRepository
import com.ea.oshop.model.Product
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
//class ProductService(@Qualifier("mockProductDataSource") private val productRepository: ProductRepository) {
class ProductService(@Qualifier("productDataSource") private val productRepository: ProductRepository) {

    fun getProducts(): List<Product> = productRepository.findAll()

    fun getProductById(id: Long): Product = productRepository.findById(id)

    fun getProductsByCategory(categoryName: String) : List<Product> = productRepository.findByCategory(categoryName)

    fun getProductsSortedBy(field: String): List<Product> =
        productRepository.findAll(Sort.by(Sort.Direction.ASC, field)) // ASC is default

    fun getProductsWithPagination(pageNumber: Int, pageSize: Int): Page<Product> =
        productRepository.findAll(PageRequest.of(pageNumber, pageSize))

    fun getProductsSortedWithPagination(pageNumber: Int, pageSize: Int, field: String): Page<Product> =
        productRepository.findAll(PageRequest.of(pageNumber, pageSize).withSort(Sort.by(field)))

    fun addProduct(product: Product) : Product = productRepository.save(product);

    fun addAllProducts(products: List<Product>) : List<Product> = productRepository.saveAll(products);

    fun updateProduct(product: Product): Product = productRepository.update(product)

    fun deleteProductById(productId: Long) = productRepository.deleteById(productId)

}