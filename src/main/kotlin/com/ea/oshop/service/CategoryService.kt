package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.CategoryRepository
import com.ea.oshop.model.Category
import org.springframework.stereotype.Service

@Service
class CategoryService(private val categoryRepository: CategoryRepository) {

    fun addCategories(categories: List<Category>): List<Category> = categoryRepository.saveAll(categories)

    fun getCategories() : List<Category> = categoryRepository.findAll()

}