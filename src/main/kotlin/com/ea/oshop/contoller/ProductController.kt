package com.ea.oshop.contoller

import com.ea.oshop.model.Product
import com.ea.oshop.service.ProductService
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = ["http://localhost:4200"])
class ProductController(private val productService: ProductService) {

    @GetMapping
    fun getProducts() : List<Product> = productService.getProducts()

    @GetMapping("/{id}")
    fun getProductById(@PathVariable ("id") id: Long) : Product = productService.getProductById(id)

    @GetMapping("/category/{name}")
    fun getProductByCategory(@PathVariable ("name") categoryName: String) : List<Product> =
        productService.getProductsByCategory(categoryName)

    @GetMapping("/sort/{field}")
    fun getProductsSortedBy(@PathVariable ("field") field: String) : List<Product> =
        productService.getProductsSortedBy(field)

    @GetMapping("/page/{page}/page-size/{pageSize}")
    fun getProductsWithPagination(
        @PathVariable ("page") page: Int,
        @PathVariable ("pageSize") pageSize: Int
    ) : Page<Product> = productService.getProductsWithPagination(page, pageSize)

    @GetMapping("/page/{page}/page-size/{pageSize}/field/{field}")
    fun getProductsSortedWithPagination(
        @PathVariable ("page") page: Int,
        @PathVariable ("pageSize") pageSize: Int,
        @PathVariable ("field") field: String
    ) : Page<Product> = productService.getProductsSortedWithPagination(page, pageSize, field)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addProduct(@RequestBody product: Product) : Product = productService.addProduct(product)

    @PostMapping("/all")
    @ResponseStatus(HttpStatus.CREATED)
    fun addProducts(@RequestBody products: List<Product>) : List<Product> = productService.addAllProducts(products)

    @PatchMapping
    fun updateProduct(@RequestBody product: Product): Product = productService.updateProduct(product)

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteProductById(@PathVariable ("id") id: Long) : Unit = productService.deleteProductById(id)
}