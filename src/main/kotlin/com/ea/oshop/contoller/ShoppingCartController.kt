package com.ea.oshop.contoller

import com.ea.oshop.dto.ShoppingCartDTO
import com.ea.oshop.service.ShoppingCartService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/shopping-carts")
@CrossOrigin(origins = ["http://localhost:4200"])
class ShoppingCartController(private val shoppingCartService: ShoppingCartService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addShoppingCart() : ShoppingCartDTO = shoppingCartService.addShoppingCart()

    @GetMapping
    fun getShoppingCarts() : List<ShoppingCartDTO> = shoppingCartService.getShoppingCarts()

    @GetMapping("/{id}")
    fun getShoppingCart(@PathVariable ("id") id: Long) : ShoppingCartDTO = shoppingCartService.getShoppingCart(id)

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun clearShoppingCart(@PathVariable ("id") id: Long) : ShoppingCartDTO = shoppingCartService.clearShoppingCart(id)
}