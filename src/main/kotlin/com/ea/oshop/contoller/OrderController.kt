package com.ea.oshop.contoller

import com.ea.oshop.dto.OrderDTO
import com.ea.oshop.service.OrderService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/orders")
@CrossOrigin(origins = ["http://localhost:4200"])
class OrderController(private val orderService: OrderService)  {

    @GetMapping
    fun getAllOrders() : List<OrderDTO> = orderService.getAllOrders()

    @GetMapping("/{id}")
    fun getOrder(@PathVariable ("id") orderId: Long) : OrderDTO = orderService.getOrder(orderId)

    @GetMapping("user/{userId}")
    fun getOrdersByUser(@PathVariable ("userId") userId: Long) : List<OrderDTO> =
        orderService.getOrdersByUser(userId)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addOrder(@RequestBody order: OrderDTO) : OrderDTO = orderService.addOrder(order)
}
