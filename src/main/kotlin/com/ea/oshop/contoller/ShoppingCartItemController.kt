package com.ea.oshop.contoller

import com.ea.oshop.dto.ShoppingCartItemDTO
import com.ea.oshop.service.ShoppingCartItemService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/items")
@CrossOrigin(origins = ["http://localhost:4200"])
class ShoppingCartItemController(private val shoppingCartItemService: ShoppingCartItemService) {

    @GetMapping
    fun getAllItems() : List<ShoppingCartItemDTO> = shoppingCartItemService.getAllItems()

    @GetMapping("/cart/{cartId}")
    fun getItemsByCartID(@PathVariable("cartId") cartId: Long) : List<ShoppingCartItemDTO> =
        shoppingCartItemService.getItemsByCartId(cartId)

    @GetMapping("/cart/{cartId}/product/{productId}")
    fun getItem(@PathVariable("cartId") cartId: Long, @PathVariable("productId") productId: Long) : ShoppingCartItemDTO =
        shoppingCartItemService.getItem(cartId, productId)

    @PostMapping("/cart/{cartId}/product/{productId}")
    @ResponseStatus(HttpStatus.CREATED)
    fun addItem(@PathVariable("cartId") cartId: Long, @PathVariable("productId") productId: Long) : ShoppingCartItemDTO =
        shoppingCartItemService.addItem(cartId, productId)

    @DeleteMapping("/cart/{cartId}/product/{productId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteItem(@PathVariable("cartId") cartId: Long, @PathVariable("productId") productId: Long) : Boolean =
        shoppingCartItemService.deleteItem(cartId, productId)

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteAllItems() : Unit = shoppingCartItemService.deleteAllItems()
}