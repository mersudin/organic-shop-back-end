package com.ea.oshop.contoller

import com.ea.oshop.dto.CredentialsDTO
import com.ea.oshop.model.User
import com.ea.oshop.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
@RequestMapping("/users")
@CrossOrigin(origins = ["http://localhost:4200"])
class UserController(private val userService: UserService) {

    @GetMapping
    fun getUsers() : List<User> = userService.getUsers()

    @GetMapping("/{userId}")
    fun getUser(@PathVariable("userId") id: Long) : User = userService.getUser(id)

    @PostMapping("/email")
    fun existingEmail(@RequestBody email: String) : Boolean = userService.existUserByEmail(email)

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    fun register(@RequestBody user: User) : String = userService.register(user)

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.CREATED)
    fun login(@RequestBody body: CredentialsDTO): String = userService.login(body)

    @DeleteMapping("/delete/{userId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteUser(@PathVariable("userId") id : Long) : Unit = userService.deleteUser(id)
}

