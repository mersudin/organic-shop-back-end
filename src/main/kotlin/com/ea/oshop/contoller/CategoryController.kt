package com.ea.oshop.contoller

import com.ea.oshop.model.Category
import com.ea.oshop.service.CategoryService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/categories")
@CrossOrigin(origins = ["http://localhost:4200"])
class CategoryController(private val categoryService: CategoryService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addCategories(@RequestBody categories: List<Category>) :List<Category> =
        categoryService.addCategories(categories)

    @GetMapping
    fun getCategories() : List<Category> = categoryService.getCategories()

}