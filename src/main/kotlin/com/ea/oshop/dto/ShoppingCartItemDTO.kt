package com.ea.oshop.dto

import com.ea.oshop.model.Product

data class ShoppingCartItemDTO(
    val id: Long,
    val product: Product,
    var quantity: Int
)
