package com.ea.oshop.dto

import java.time.LocalDate

data class ShoppingCartDTO(
    val id: Long,
    val dateCreated: LocalDate,
    val items: List<ShoppingCartItemDTO>
)
