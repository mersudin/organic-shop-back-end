package com.ea.oshop.dto

data class CredentialsDTO(
    val email: String,
    val password: String
) {}