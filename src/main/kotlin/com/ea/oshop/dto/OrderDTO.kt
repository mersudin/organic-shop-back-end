package com.ea.oshop.dto

import com.ea.oshop.model.Address

data class OrderDTO (
    val id: Long,
    val userId: Long,
    var datePlaced: String,
    val shoppingCart: ShoppingCartDTO,
    val shippingAddress: Address
)