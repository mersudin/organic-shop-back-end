package com.ea.oshop.dataSource

import com.ea.oshop.dataSource.repository.DBShoppingCartItemRepository
import com.ea.oshop.dataSource.repository.ShoppingCartItemRepository
import com.ea.oshop.model.ShoppingCartItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*

@Repository("itemDataSource")
class ShoppingCartItemDataSource (@Autowired val dbShoppingCartItemRepository: DBShoppingCartItemRepository) : ShoppingCartItemRepository {
    override fun findAll(): List<ShoppingCartItem> = dbShoppingCartItemRepository.findAll()

    override fun findById(id: Long): ShoppingCartItem = dbShoppingCartItemRepository.findById(id)
        .orElseThrow {
            throw NoSuchElementException("Could not find item with id $id")
        }

    override fun findByShoppingCartId(cartId: Long): List<ShoppingCartItem> = dbShoppingCartItemRepository.findByShoppingCartId(cartId)
        .orElseThrow {
            throw NoSuchElementException("Could not find items with cart id $cartId")
        }
    override fun findByShoppingCartIdAndProductId(cartId: Long, productId: Long): ShoppingCartItem =
        dbShoppingCartItemRepository.findByShoppingCartIdAndProductId(cartId, productId)
            .orElseThrow {
                throw NoSuchElementException("Could not find item with cart id $cartId and product id $productId")
            }
    override fun save(item: ShoppingCartItem): ShoppingCartItem = dbShoppingCartItemRepository.save(item)

    override fun delete(item: ShoppingCartItem) = dbShoppingCartItemRepository.delete(item)

    override fun deleteAll() = dbShoppingCartItemRepository.deleteAll()

    override fun exists(cartId: Long, productId: Long) : Boolean = dbShoppingCartItemRepository.findByShoppingCartIdAndProductId(cartId, productId).isPresent
}