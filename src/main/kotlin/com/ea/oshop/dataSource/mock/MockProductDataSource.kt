package com.ea.oshop.dataSource.mock

import com.ea.oshop.dataSource.repository.ProductRepository
import com.ea.oshop.model.Product
import org.springframework.data.domain.*
import org.springframework.stereotype.Repository
import java.util.*


@Repository("mockProductDataSource")
class MockProductDataSource : ProductRepository {
    val products = mutableListOf(
        Product(1, "Apple", 7.0, "Fruits", "url1"),
        Product(2, "Spinach", 3.3, "Vegetables", "url3"),
        Product(3, "Banana", 2.2, "Fruits", "url2"),
        Product(4, "Milk", 4.4, "Dairy", "url4"),
        Product(5, "Cinnamon sticks", 5.5, "Seasonings and Spices", "url5"),
        Product(6, "Homemade", 6.6, "Bread", "url6"),
        Product(7, "Apple2", 11.0, "Fruits", "url1"),
        Product(8, "Peach", 5.2, "Fruits", "url2"),
        Product(9, "Tomato", 13.3, "Vegetables", "url3"),
        Product(10, "Milk", 15.4, "Dairy", "url4"),
        Product(11, "Cinnamon sticks2", 14.5, "Seasonings and Spices", "url5"),
        Product(12, "Freshly baked", 1.6, "Bread", "url6")
    )

    override fun findAll(): List<Product> = products

    override fun findAll(sort: Sort): List<Product> {
        val order = sort.first()
        return when (order.property) {
            "title" -> products.sortedWith(compareBy { it.title })
            "category" -> products.sortedWith(compareBy { it.category })
            "price" -> products.sortedWith(compareBy { it.price })
            else -> products
        }
    }

    override fun findAll(pageRequest: PageRequest): Page<Product> {
        val start = pageRequest.offset
        val end = (start + pageRequest.pageSize).toInt().coerceAtMost(products.size)
        if (start > products.size)
            throw IllegalArgumentException("Out of range parameters: pageNumber=${pageRequest.pageNumber} , pageSize=${pageRequest.pageSize}")

        var products = this.products.toList()
        if(pageRequest.sort.firstOrNull() != null )
            products = findAll(pageRequest.sort)

        return PageImpl(products.subList(start.toInt(), end), pageRequest, products.size.toLong())
    }

    override fun findById(id: Long): Product = products.firstOrNull {it.id == id}
        ?: throw NoSuchElementException("Could not find a product with ID $id")

    override fun findByCategory(categoryName: String): List<Product> {
        val filteredProducts = products.filter{ it.category == categoryName }
        if(filteredProducts.isEmpty())
            throw NoSuchElementException("Could not find products in category $categoryName")
        return filteredProducts
    }

    override fun save(product: Product): Product {
        if(existsById(product.id))
            throw IllegalArgumentException("Product with id ${product.id} already exists")
        products.add(product)
        return product
    }

    override fun saveAll(p: List<Product>): List<Product> {
        p.forEach {
            if(existsById(it.id))
                throw IllegalArgumentException("Product with id ${it.id} already exists")
            save(it)
        }
        return products
    }

    override fun update(product: Product): Product {
        val index = products.indexOfFirst { it.id == product.id  }
        if (index == -1)
            throw NoSuchElementException("Product with id ${product.id} does not exists")
        products[index] = product
        return products[index]
    }

    override fun deleteById(id: Long) {
        val product = findById(id)
        products.remove(product)
    }

    override fun existsById(id: Long): Boolean {
        val product = products.firstOrNull {it.id == id}
        return product != null
    }

}