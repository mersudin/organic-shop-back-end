package com.ea.oshop.dataSource.mock

import com.ea.oshop.dataSource.repository.UserRepository
import com.ea.oshop.model.User
import org.springframework.stereotype.Repository

@Repository("mockUserDataSource")
class MockUserDataSource : UserRepository {
    // ENCRYPTED.Password = $2a$10$/cNgDwmUPEUv/kbU/55Oz.c0LqsfGWPcIXDLGhHVOjHSaTVUx3QQi
    private val admin =  User(101, "Admin", "admin@domain.com", "\$2a\$10\$/cNgDwmUPEUv/kbU/55Oz.c0LqsfGWPcIXDLGhHVOjHSaTVUx3QQi", true)
    private val regularUser = User(102, "User", "user@domain.com", "\$2a\$10\$/cNgDwmUPEUv/kbU/55Oz.c0LqsfGWPcIXDLGhHVOjHSaTVUx3QQi", false)
    val users = mutableListOf(admin, regularUser)
    override fun findAll(): List<User> = users

    override fun findById(id: Long): User = users.firstOrNull {it.id == id}
        ?: throw NoSuchElementException("Could not find user with ID $id")

    override fun findByEmail(email: String): User = users.firstOrNull {it.email == email}
        ?: throw NoSuchElementException("Could not find user with email $email")

    override fun existsByEmail(email: String): Boolean {
//        val user = users.find { it.email == email }
        val user = users.firstOrNull { it.email == email }
        return user != null
    }

    override fun save(user: User): User {
        if (existsByEmail(user.email))
            throw IllegalArgumentException("User with email ${user.email} already exist")

        users.add(user)
        return user
    }

    override fun deleteById(id: Long) {
        val user = findById(id)
        users.remove(user)
    }
}