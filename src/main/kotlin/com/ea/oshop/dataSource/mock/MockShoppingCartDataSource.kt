package com.ea.oshop.dataSource.mock

import com.ea.oshop.dataSource.repository.ShoppingCartRepository
import com.ea.oshop.model.ShoppingCart
import org.springframework.stereotype.Repository
import java.time.LocalDate
import java.util.*

@Repository("mockCartDataSource")
class MockShoppingCartDataSource : ShoppingCartRepository {
    val carts = mutableListOf(
        ShoppingCart(1000, LocalDate.of(2020,1,1)),
        ShoppingCart(2000, LocalDate.of(2020,2,2)),
        ShoppingCart(3000, LocalDate.of(2020,3,3)),
        ShoppingCart(4000, LocalDate.of(2020,4,4))
    )

    override fun findAll() : MutableList<ShoppingCart> = carts

    override fun findById(id: Long): ShoppingCart = carts.firstOrNull {it.id == id}
            ?: throw NoSuchElementException("Could not find a shopping cart with id $id")

    override fun save(cart: ShoppingCart) : ShoppingCart {
        val newCart = ShoppingCart(carts.size + 1L, cart.dateCreated)
        carts.add(newCart)

        return newCart
    }
}