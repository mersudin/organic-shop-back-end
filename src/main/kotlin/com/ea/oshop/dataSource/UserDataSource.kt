package com.ea.oshop.dataSource

import com.ea.oshop.dataSource.repository.DBUserRepository
import com.ea.oshop.dataSource.repository.UserRepository
import com.ea.oshop.model.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository("userDataSource")
class UserDataSource (@Autowired val dbUserRepository: DBUserRepository) : UserRepository {
    override fun findAll(): List<User> = dbUserRepository.findAll()

    override fun findById(id: Long): User = dbUserRepository.findById(id)
        .orElseThrow {
            throw NoSuchElementException("Could not find user with id $id")
        }

    override fun findByEmail(email: String): User = dbUserRepository.findByEmail(email)
        .orElseThrow {
            throw NoSuchElementException("Could not find user with email $email")
        }
    override fun existsByEmail(email: String): Boolean = dbUserRepository.findByEmail(email).isPresent

    override fun save(user: User): User {
        if(existsByEmail(user.email))
            throw IllegalArgumentException("User with email ${user.email} already exist")

        return dbUserRepository.save(user)
    }

    override fun deleteById(id: Long) {
        findById(id)
        dbUserRepository.deleteById(id)
    }
}