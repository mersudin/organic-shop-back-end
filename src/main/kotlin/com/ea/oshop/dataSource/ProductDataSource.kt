package com.ea.oshop.dataSource

import com.ea.oshop.dataSource.repository.DBProductRepository
import com.ea.oshop.dataSource.repository.ProductRepository
import com.ea.oshop.model.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Repository

@Repository("productDataSource")
class ProductDataSource(@Autowired val dbProductRepository: DBProductRepository) : ProductRepository {

    override fun findAll(): List<Product> = dbProductRepository.findAll()

    override fun findAll(sort: Sort): List<Product> = dbProductRepository.findAll(sort)

    override fun findAll(pageRequest: PageRequest): Page<Product> = dbProductRepository.findAll(pageRequest)

    override fun findById(id: Long): Product = dbProductRepository.findById(id)
        .orElseThrow {
            throw NoSuchElementException("Could not find product with id $id")
        }

    override fun findByCategory(categoryName: String): List<Product> = dbProductRepository.findByCategory(categoryName)
        .orElseThrow {
            throw NoSuchElementException("Could not find products with category $categoryName")
        }

    override fun save(product: Product): Product = dbProductRepository.save(product)

    override fun saveAll(products: List<Product>): List<Product> {
        products.forEach {
            if(existsById(it.id))
                throw IllegalArgumentException("Product with id ${it.id} already exists")
            dbProductRepository.save(it)
        }
        return products
    }

    override fun update(product: Product): Product {
        if(!existsById(product.id))
            throw IllegalArgumentException("Product with id ${product.id} does not exists")
        return save(product)
    }

    override fun deleteById(id: Long) {
        findById(id)
        dbProductRepository.deleteById(id)
    }

    override fun existsById(id: Long): Boolean = dbProductRepository.existsById(id)

}


