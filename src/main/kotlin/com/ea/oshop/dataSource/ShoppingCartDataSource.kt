package com.ea.oshop.dataSource

import com.ea.oshop.dataSource.repository.DBShoppingCartRepository
import com.ea.oshop.dataSource.repository.ShoppingCartRepository
import com.ea.oshop.model.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.util.*

@Repository("cartDataSource")
class ShoppingCartDataSource(@Autowired val dbShoppingCartRepository: DBShoppingCartRepository) : ShoppingCartRepository {
    override fun findAll() : List<ShoppingCart> = dbShoppingCartRepository.findAll()
    override fun save(cart: ShoppingCart) : ShoppingCart = dbShoppingCartRepository.save(cart)
    override fun findById(id: Long) : ShoppingCart = dbShoppingCartRepository.findById(id)
        .orElseThrow {
            throw NoSuchElementException("Could not find a shopping cart with id $id")
        }
}