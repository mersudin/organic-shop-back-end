package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.ShoppingCartItem

interface ShoppingCartItemRepository {
    fun findAll() : List<ShoppingCartItem>
    fun findById(id: Long) : ShoppingCartItem
    fun findByShoppingCartId(cartId: Long) : List<ShoppingCartItem>
    fun findByShoppingCartIdAndProductId(cartId: Long, productId: Long) : ShoppingCartItem
    fun exists(cartId: Long, productId: Long) : Boolean
    fun save(item: ShoppingCartItem) : ShoppingCartItem
    fun delete(item: ShoppingCartItem)
    fun deleteAll()
}