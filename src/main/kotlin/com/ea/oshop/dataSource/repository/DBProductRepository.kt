package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DBProductRepository : JpaRepository<Product, Long> {
    fun findByCategory(categoryName: String) : Optional<List<Product>>

}