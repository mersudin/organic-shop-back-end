package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DBUserRepository : JpaRepository<User, Long> {
    fun findByEmail(email: String): Optional<User>

}