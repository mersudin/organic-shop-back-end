package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.Product
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository {
    fun findAll() : List<Product>
    fun findAll(sort: Sort) : List<Product>
    fun findAll(pageRequest: PageRequest) : Page<Product>
    fun findById(id: Long) : Product
    fun findByCategory(categoryName: String) : List<Product>
    fun save(product: Product) : Product
    fun saveAll(products: List<Product>) : List<Product>
    fun update(product: Product) : Product
    fun deleteById(id: Long)
    fun existsById(id: Long) : Boolean
}