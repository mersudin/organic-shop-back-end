package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.Order
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderRepository : JpaRepository<Order, Long> {

    fun findByUserId(userId: Long) : List<Order>;
}