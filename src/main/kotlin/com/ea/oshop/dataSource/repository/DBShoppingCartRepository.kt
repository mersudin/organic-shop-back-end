package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.ShoppingCart
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DBShoppingCartRepository : JpaRepository<ShoppingCart, Long>{
}