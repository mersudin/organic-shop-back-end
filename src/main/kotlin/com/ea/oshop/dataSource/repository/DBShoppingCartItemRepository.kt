package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.ShoppingCartItem
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface DBShoppingCartItemRepository : JpaRepository<ShoppingCartItem, Long> {

    fun findByShoppingCartId(id: Long) : Optional<List<ShoppingCartItem>>;

//    @org.springframework.lang.Nullable
    @Query("select * from shopping_cart_item s where s.shopping_cart_id = ?1 AND s.product_id = ?2", nativeQuery = true)
    fun findByShoppingCartIdAndProductId(cartId: Long, productId: Long) : Optional<ShoppingCartItem>;

}