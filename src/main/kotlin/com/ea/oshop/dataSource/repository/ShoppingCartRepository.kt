package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.ShoppingCart

interface ShoppingCartRepository {
    fun findAll(): List<ShoppingCart>
    fun findById(id: Long) : ShoppingCart
    fun save(cart: ShoppingCart) : ShoppingCart
}