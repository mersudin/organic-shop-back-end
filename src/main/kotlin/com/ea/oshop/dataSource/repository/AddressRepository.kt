package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.Address
import org.springframework.data.jpa.repository.JpaRepository

interface AddressRepository : JpaRepository<Address, Long> {
}