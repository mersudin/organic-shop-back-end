package com.ea.oshop.dataSource.repository

import com.ea.oshop.model.User

interface UserRepository {
    fun findAll() : List<User>
    fun findById(id: Long) : User
    fun findByEmail(email: String) : User
    fun existsByEmail(email: String) : Boolean
    fun save(user: User) : User
    fun deleteById(id: Long)
}