package com.ea.oshop

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.runApplication

//@SpringBootApplication
@SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
class OshopApplication

fun main(args: Array<String>) {
	runApplication<OshopApplication>(*args)
}
