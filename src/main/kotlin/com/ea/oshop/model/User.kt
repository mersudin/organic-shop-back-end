package com.ea.oshop.model

import javax.persistence.*

@Entity
@Table(name="\"user\"")
data class User (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val name: String,
    val email: String,
    var password: String,
    val isAdmin: Boolean,
)
