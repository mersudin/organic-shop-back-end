package com.ea.oshop.model

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name="\"order\"") // "order" is reserved keyword
data class Order(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val userId: Long,
    val datePlaced: LocalDate,
    val shoppingCartId: Long,
    val shippingAddressId: Long,
)