package com.ea.oshop.model

import javax.persistence.*

@Entity
@Table
data class Address (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long,
    val name: String,
    val addressLine1: String,
    val addressLine2: String,
    val city: String
)