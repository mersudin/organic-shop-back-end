package com.ea.oshop.model

import javax.persistence.*

@Entity
@Table
data class Category(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val name: String
)
