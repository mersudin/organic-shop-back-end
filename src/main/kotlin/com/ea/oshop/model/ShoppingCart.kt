package com.ea.oshop.model

import java.time.LocalDate
import javax.persistence.*

@Entity
@Table
data class ShoppingCart(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,
    val dateCreated: LocalDate
)
