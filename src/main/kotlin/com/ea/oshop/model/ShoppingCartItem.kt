package com.ea.oshop.model

import javax.persistence.*

@Entity
@Table
data class ShoppingCartItem (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = 0,
    val shoppingCartId: Long,
    val productId: Long,
    var quantity: Int
) {}
