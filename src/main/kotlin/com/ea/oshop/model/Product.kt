package com.ea.oshop.model

import javax.persistence.*

@Entity
@Table
data class Product (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val title: String,
    val price: Double,
    val category: String,
    val imageUrl: String
)