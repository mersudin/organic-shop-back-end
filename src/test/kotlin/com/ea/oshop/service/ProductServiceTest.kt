package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.ProductRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class ProductServiceTest {
    private val productRepository: ProductRepository = mockk(relaxed = true)
    private val productService = ProductService(productRepository)

    @Test
    fun `should call its data source to retrieve products` () {
        // when
        productService.getProducts()

        // then
        verify(exactly = 1) { productRepository.findAll() }
    }

}