package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.ShoppingCartRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class ShoppingCartServiceTest {
    private val shoppingCartRepository: ShoppingCartRepository = mockk(relaxed = true)
    private val shoppingCartItemService: ShoppingCartItemService = mockk(relaxed = true)
    private val shoppingCartService = ShoppingCartService(shoppingCartRepository, shoppingCartItemService)

    @Test
    fun `should call its data source to retrieve carts` () {
        // when
        shoppingCartService.getShoppingCarts()

        // then
        verify(exactly = 1) {shoppingCartRepository.findAll()}
    }

}