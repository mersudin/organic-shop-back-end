package com.ea.oshop.service

import com.ea.oshop.dataSource.repository.UserRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class UserServiceTest {
    private val userRepository: UserRepository = mockk(relaxed = true)
    private val userService = UserService(userRepository)

    @Test
    fun `should call its data source to retrieve users` () {
        // when
        userService.getUsers()

        // then
        verify(exactly = 1) { userRepository.findAll() }
    }
}