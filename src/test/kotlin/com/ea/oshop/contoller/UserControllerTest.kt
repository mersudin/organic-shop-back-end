package com.ea.oshop.contoller

import com.ea.oshop.dto.CredentialsDTO
import com.ea.oshop.model.User
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

@SpringBootTest
@AutoConfigureMockMvc
internal class UserControllerTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper
) {
    private val baseUrl = "/users"

    @Nested
    @DisplayName("GET /users")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetUsers {
        @Test
        fun `should ` () {
            // when/then
            mockMvc.get(baseUrl)
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.length()") { value(2) }
                    jsonPath("$[0].id") { value(101) }
                    jsonPath("$[1].id") { value(102) }
                }
        }
    }
    
    @Nested
    @DisplayName("GET /users")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetUser {
        @Test
        fun `should return user with giveb id` () {
            // given
            val id = 101
            
            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.id") { value(id) }
                }
        }
        @Test
        fun `should return NOT FOUND if user with given id does not exist` () {
            // given
            val id = 999

            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect { status { isNotFound() } }
        }
    }

    @Nested
    @DisplayName("POST /users/email")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class ExistingEmail {
        @Test
        fun `should return true when email exist` () {
            // given
            val existingEmail = "user@domain.com"

            // when
            val performPost = mockMvc.post("$baseUrl/email") {
                contentType = MediaType.APPLICATION_JSON
                content = existingEmail
            }

            performPost
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        jsonPath("$") { value (true) }
                    }
                }
        }
        @Test
        fun `should return false if email doesnt exist` () {
            // given
            val unknownEmail = "unknown_email"

            // when
            val performPost = mockMvc.post("$baseUrl/email") {
                contentType = MediaType.APPLICATION_JSON
                content = unknownEmail
            }

            performPost
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        jsonPath("$") { value (false) }
                    }
                }
        }
    }

    @Nested
    @DisplayName("POST /users/register")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class Register {
        @Test
        @DirtiesContext
        fun `should register new user and return the token` () {
            val newUser = User(201, "User201", "user201@domain.com", "ENCRYPTED.Password", false)

            val performPost = mockMvc.post("$baseUrl/register") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(newUser)
            }

            performPost
                .andDo { print() }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType(MediaType.TEXT_PLAIN, Charsets.UTF_8))
                    }
                }

            mockMvc.get("$baseUrl/${newUser.id}")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.id") { value(newUser.id) }
                }
        }
        @Test
        fun `should return BAD REQUEST if user already exists` () {
            val existingUser = User(102, "User", "user@domain.com", "ENCRYPTED.Password", false)

            val performPost = mockMvc.post("$baseUrl/register") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(existingUser)
            }

            performPost
                .andDo { print() }
                .andExpect { status { isBadRequest() } }
        }
    }

    @Nested
    @DisplayName("POST /users/login")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class Login {
        @Test
        fun `should return token if valid credentials are provided` () {
            val credentials = CredentialsDTO("user@domain.com", "ENCRYPTED.Password")

            val performPost = mockMvc.post("$baseUrl/login") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(credentials)
            }

            performPost
                .andDo { print() }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType(MediaType.TEXT_PLAIN, Charsets.UTF_8))
                    }
                }
        }
        @Test
        fun `should return NOT FOUND if user is not registered with given email` () {
            val credentials = CredentialsDTO("unknown_email", "ENCRYPTED.Password")

            val performPost = mockMvc.post("$baseUrl/login") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(credentials)
            }

            performPost
                .andDo { print() }
                .andExpect { status { isNotFound() } }
        }
        @Test
        fun `should return BAD REQUEST passwords do not match` () {
            val credentials = CredentialsDTO("user@domain.com", "invalid_password")

            val performPost = mockMvc.post("$baseUrl/login") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(credentials)
            }

            performPost
                .andDo { print() }
                .andExpect { status { isBadRequest() } }
        }
    }
    
    @Nested
    @DisplayName("DELETE /users/delete/{id}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class DeleteUser {
        @Test
        fun `should delete user with given id` () {
            val validId = 102

            mockMvc.delete("$baseUrl/delete/$validId")
                .andDo { print() }
                .andExpect { status { isNoContent() } }
            mockMvc.get("$baseUrl/$validId")
                .andExpect { status { isNotFound() } }
        }
        @Test
        fun `should return NOT FOUND when trying to remove user that doesnt exist` () {
            // given
            val unknownUserId = 300

            // when
            mockMvc.delete("$baseUrl/delete/$unknownUserId")
                .andDo { print() }
                .andExpect { status { isNotFound() } }
        }
    }
}