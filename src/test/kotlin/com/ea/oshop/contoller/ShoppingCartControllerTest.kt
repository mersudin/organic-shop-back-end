package com.ea.oshop.contoller

import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import java.time.LocalDate

@SpringBootTest
@AutoConfigureMockMvc
internal class ShoppingCartControllerTest (
    @Autowired val mockMvc: MockMvc
) {
    private val baseUrl = "/shopping-carts"

    @Nested
    @DisplayName("GET /shopping-carts")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetShoppingCarts {
        @Test
        fun `should return all shopping carts` () {
            // when/then
            mockMvc.get(baseUrl)
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.length()") { value(4) }
                    jsonPath("$[0].id") { value(1000) }
                    jsonPath("$[1].id") { value(2000) }
                    jsonPath("$[2].id") { value(3000) }
                    jsonPath("$[3].id") { value(4000) }
                }
        }
    }

    @Nested
    @DisplayName("GET /shopping-carts/{id}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetShoppingCart {
        @Test
        fun `should return the shopping cart with given id` () {
            // given
            val id = 3000
            val date = LocalDate.of(2020,3,3)

            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.id") { value(id) }
                    jsonPath("$.dateCreated") { value(date.toString()) }
                }
        }

        @Test
        fun `should return NOT FOUND if the shopping cart with given id does not exists` () {
            // given
            val id = 100

            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect { status { isNotFound() } }
        }

        @Test
        fun `should return BAD REQUEST if the id is invalid` () {
            // given
            val id = "invalid_id"

            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect { status { isBadRequest() } }
        }
    }

    @Nested
    @DisplayName("POST /shopping-carts")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class PostNewCart {
        private val newID = 5
        private val todayDate = LocalDate.now()
        @Test
        fun `should add new cart` () {
            // when/then
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andDo { print() }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        jsonPath("$.dateCreated") { value(todayDate.toString()) }
                    }
                }
            mockMvc.get("$baseUrl/$newID")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.id") { value(newID) }
                    jsonPath("$.dateCreated") { value(todayDate.toString()) }
                }
        }
    }

}