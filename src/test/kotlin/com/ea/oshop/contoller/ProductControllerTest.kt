package com.ea.oshop.contoller

import com.ea.oshop.model.Product
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.*

@SpringBootTest
@AutoConfigureMockMvc
internal class ProductControllerTest @Autowired constructor (
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper
) {
    private val baseUrl = "/products"

    @Nested
    @DisplayName("GET /products")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetProducts {
        @Test
        fun `should return all products` () {
            // when/then
            mockMvc.get(baseUrl)
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.length()") { value(12) }
                    jsonPath("$[0].id") { value(1) }
                    jsonPath("$[11].id") { value(12) }
                }
        }
    }
    
    @Nested
    @DisplayName("GET /products/{id}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetProduct {
        @Test
        fun `should return product with given id` () {
            // given
            val id = 5

            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.id") { value(id) }
                }
        }
        @Test
        fun `should return NOT FOUND if product with given id does not exists` () {
            // given
            val id = 100
            
            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect {status { isNotFound() } }
        }
        @Test
        fun `should return BAD REQUEST if the id is invalid` () {
            // given
            val id = "invalid_id"

            // when/then
            mockMvc.get("$baseUrl/$id")
                .andDo { print() }
                .andExpect { status { isBadRequest() } }
        }
    }

    @Nested
    @DisplayName("GET /products/category/{name}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetProductByCategory {
        private val category = "Fruits"
        @Test
        fun `should return all products in category` () {
            // when/then
            mockMvc.get("$baseUrl/category/$category")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.length()") { value(4) }
                    jsonPath("$[0].category") { value (category) }
                    jsonPath("$[1].category") { value (category) }
                    jsonPath("$[2].category") { value (category) }
                    jsonPath("$[3].category") { value (category) }
                }
        }
        @Test
        fun `should return NOT FOUND if there is no products in given category` () {
            // given
            val invalidCategory = "invalid_category"

            // when/then
            mockMvc.get("$baseUrl/category/$invalidCategory")
                .andDo { print() }
                .andExpect {status { isNotFound()} }
        }
    }
    
    @Nested
    @DisplayName("GET /products/sort/{field}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetProductsSortedBy {
        @Test
        fun `should return sorted products by title` () {
            val field = "title"

            mockMvc.get("$baseUrl/sort/$field")
               .andDo { print() }
               .andExpect {
                   status { isOk() }
                   content { contentType(MediaType.APPLICATION_JSON) }
                   jsonPath("$.length()") { value (12) }
                   jsonPath("$[0].title") { value("Apple") }
                   jsonPath("$[1].title") { value("Apple2") }
                   jsonPath("$[10].title") { value("Spinach") }
                   jsonPath("$[11].title") { value("Tomato") }
               }
        }
        @Test
        fun `should return sorted products by price` () {
            val field = "price"

            mockMvc.get("$baseUrl/sort/$field")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.length()") { value (12) }
                    jsonPath("$[0].price") { value(1.6) }
                    jsonPath("$[1].price") { value(2.2) }
                    jsonPath("$[10].price") { value(14.5) }
                    jsonPath("$[11].price") { value(15.4) }
                }
        }
        @Test
        fun `should return sorted products by category` () {
            val field = "category"

            mockMvc.get("$baseUrl/sort/$field")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.length()") { value (12) }
                    jsonPath("$[0].category") { value("Bread") }
                    jsonPath("$[1].category") { value("Bread") }
                    jsonPath("$[10].category") { value("Vegetables") }
                    jsonPath("$[11].category") { value("Vegetables") }
                }
        }
    }
    
    @Nested
    @DisplayName("GET /products/offset/{offset}/page-size/{pageSize}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetProductsWithPagination {
        @Test
        fun `should return products with pagination` () {
            // given
            val page = 1
            val pageSize = 2

            // when
            mockMvc.get("$baseUrl/page/$page/page-size/$pageSize")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.content.length()") { value(pageSize) }
                    jsonPath("$.content[0].id") { value (3) }
                    jsonPath("$.content[1].id") { value (4) }
                }
        }
        @Test
        fun `should return BAD REQUEST if parameters are out of range` () {
            // given
            val page = 3
            val pageSize = 6

            // when/then
            mockMvc.get("$baseUrl/page/$page/page-size/$pageSize")
                .andDo { print() }
                .andExpect { status { isBadRequest() } }
        }
    }

    @Nested
    @DisplayName("GET /products/page/{page}/page-size/{pageSize}/field/{field}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetProductsSortedWithPagination {
        @Test
        fun `should return sorted products by title with pagination` () {
            val page = 0
            val pageSize = 2
            val field = "title"

            mockMvc.get("$baseUrl/page/$page/page-size/$pageSize/field/$field")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.content.length()") { value(pageSize) }
                    jsonPath("$.content[0].title") { value ("Apple") }
                    jsonPath("$.content[1].title") { value ("Apple2") }

                }
        }
        @Test
        fun `should return sorted products by price with pagination` () {
            val page = 0
            val pageSize = 6
            val field = "price"

            mockMvc.get("$baseUrl/page/$page/page-size/$pageSize/field/$field")
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content { contentType(MediaType.APPLICATION_JSON) }
                    jsonPath("$.content.length()") { value (pageSize) }
                    jsonPath("$.content[0].price") { value(1.6) }
                    jsonPath("$.content[1].price") { value(2.2) }
                    jsonPath("$.content[4].price") { value(5.2) }
                    jsonPath("$.content[5].price") { value(5.5) }

                }
        }
    }
    
    @Nested
    @DisplayName("POST /products")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class AddProduct {
        @Test
        @DirtiesContext
        fun `should add the new product ` () {
            val newProduct = Product(21, "Apple", 1.1, "Fruits", "apple_url")

            val performPost = mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(newProduct)
            }

            performPost
                .andDo { print() }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(newProduct))
                    }
                }

            mockMvc.get("$baseUrl/${newProduct.id}")
                .andExpect { content { json(objectMapper.writeValueAsString(newProduct)) } }
        }
        @Test
        fun `should return BAD REQUEST if product already exists` () {
            val existingProduct = Product(1, "Apple", 7.0, "Fruits", "url1")

            val performPost = mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(existingProduct)
            }

            performPost
                .andDo { print() }
                .andExpect {status { isBadRequest() } }
        }
    }

    @Nested
    @DirtiesContext
    @DisplayName("POST /products/all")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class AddProducts {
        @Test
        fun `should add all new products` () {
            val newProducts = listOf(
                Product(100, "Cinnamon", 5.5, "Seasonings and Spices", "url5"),
                Product(101, "Bagel", 6.6, "Bread", "url6"),
                Product(102, "Grape", 11.0, "Fruits", "url1"),
            )

            val performPost = mockMvc.post("$baseUrl/all") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(newProducts)
            }

            performPost
                .andDo { print() }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
//                        json(objectMapper.writeValueAsString(newProducts))
                        jsonPath("$.length()") { value (15) }
                    }
                }
        }
        @Test
        fun `should return BAD REQUEST when adding multiple products if some product already exists` () {
            val existingProducts = listOf(
                Product(8, "Peach", 5.2, "Fruits", "url2"),
                Product(9, "Tomato", 13.3, "Vegetables", "url3"),
                Product(10, "Milk", 15.4, "Dairy", "url4"),
            )

            val performPost = mockMvc.post("$baseUrl/all") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(existingProducts)
            }

            performPost
                .andDo { print() }
                .andExpect { status { isBadRequest() } }
        }
    }

    @Nested
    @DisplayName("PATCH /products")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class UpdateProduct {
        @Test
        @DirtiesContext
        fun `should update an existing product` () {
            val updatedProduct = Product(1, "Orange", 91.1, "Vegetables", "orange_url")

            val performPost = mockMvc.patch(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updatedProduct)
            }

            performPost
                .andDo { print() }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(updatedProduct))
                    }
                }

            mockMvc.get("$baseUrl/${updatedProduct.id}")
                .andExpect { content { json(objectMapper.writeValueAsString(updatedProduct)) } }
        }
        @Test
        fun `should return BAD REQUEST if product does not exists` () {
            val invalidProduct = Product(101, "Mango", 17.0, "Fruits", "mango_url")

            val performPost = mockMvc.patch(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(invalidProduct)
            }

            performPost
                .andDo { print() }
                .andExpect {status { isNotFound() } }
        }
    }
    
    @Nested
    @DisplayName("DELETE /products/{id}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class DeleteProduct {
        @Test
        fun `should delete the product with the given id` () {
            // given
            val id = 1

            mockMvc.delete("$baseUrl/$id")
                .andDo { print() }
                .andExpect { status { isNoContent() } }
            mockMvc.get("$baseUrl/$id")
                .andExpect { status { isNotFound() } }
        }
        @Test
        fun `should return NOT FOUND if no product with given id exists` () {
            val nonExistentProductId = 222

            mockMvc.delete("$baseUrl/$nonExistentProductId")
                .andDo { print() }
                .andExpect { status { isNotFound() } }
        }
    }

}
