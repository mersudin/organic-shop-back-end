package com.ea.oshop.dataSource.mock

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MockUserDataSourceTest {
    private val mockUserDataSource = MockUserDataSource()

    @Test
    fun `should provide a list of users` () {
        // when
        val users = mockUserDataSource.findAll()

        // then
        assertThat(users.size).isEqualTo(2)
        assertThat(users[0].id).isEqualTo(101)
        assertThat(users[1].id).isEqualTo(102)
    }
}