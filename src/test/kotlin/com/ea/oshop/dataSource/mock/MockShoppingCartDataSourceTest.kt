package com.ea.oshop.dataSource.mock

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MockShoppingCartDataSourceTest {

    private val mockShoppingCartDataSource = MockShoppingCartDataSource()

   @Test
   fun `should provide a list of shopping carts` () {
       // when
       val carts = mockShoppingCartDataSource.findAll();

       // then
       assertThat(carts.size).isGreaterThanOrEqualTo(3)
   }

    @Test
    fun `should provide some mock data` () {
        // when
        val carts = mockShoppingCartDataSource.findAll()

        // then
        assertThat(carts).allMatch{ it.id != 0L }
        assertThat(carts).allMatch{ true }
    }
}