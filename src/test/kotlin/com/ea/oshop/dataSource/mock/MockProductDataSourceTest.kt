package com.ea.oshop.dataSource.mock

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MockProductDataSourceTest {
    private val mockProductDataSource = MockProductDataSource()
    
    @Test
    fun `should provide a list of products` () {
        // when
        val products = mockProductDataSource.findAll()
        
        // then
        assertThat(products.size).isGreaterThanOrEqualTo(10)
        assertThat(products[0].id).isEqualTo(1)
        assertThat(products[0].title).isEqualTo("Apple")
    }

}